//
//  DRBaseTabBarViewController.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/23.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRBaseTabBarViewController.h"
#import "DRBaseNavViewController.h"
#import "DRUserHomeViewController.h"
#import "DRMovieViewController.h"
#import "DRCinemaViewController.h"
#import "DRActiveViewController.h"


@interface DRBaseTabBarViewController ()

@end

@implementation DRBaseTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    /*
    //活动
    DRActiveViewController *active = [DRActiveViewController new];
    DRBaseNavViewController *actNav = [[DRBaseNavViewController alloc] initWithRootViewController:active];
    actNav.tabBarItem.title = @"活动";
    actNav.tabBarItem.image = [UIImage imageNamed:@"active"];
    actNav.tabBarItem.selectedImage = [[UIImage imageNamed:@"active_select"] imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)];
    
    [self addChildViewController:actNav];
    //电影
    DRMovieViewController *movie = [DRMovieViewController new];
    DRBaseNavViewController *movNav = [[DRBaseNavViewController alloc] initWithRootViewController:movie];
    movNav.tabBarItem.title = @"电影";
    movNav.tabBarItem.image = [UIImage imageNamed:@"movie"];
    movNav.tabBarItem.selectedImage = [[UIImage imageNamed:@"movie_select"]imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)];
    [self addChildViewController:movNav];
    //影院
    DRCinemaViewController *cinema = [DRCinemaViewController new];
    DRBaseNavViewController *cinNav = [[DRBaseNavViewController alloc] initWithRootViewController:cinema];
    cinNav.tabBarItem.title = @"影院";
    cinNav.tabBarItem.image = [UIImage imageNamed:@"cinema"];
    cinNav.tabBarItem.selectedImage = [[UIImage imageNamed:@"cinema_select"] imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)];
    [self addChildViewController:cinNav];
    //用户中心
    DRUserHomeViewController *user = [DRUserHomeViewController new];
    DRBaseNavViewController *userNav = [[DRBaseNavViewController alloc] initWithRootViewController:user];
    userNav.tabBarItem.title = @"用户";
    userNav.tabBarItem.image = [UIImage imageNamed:@"user"];
    userNav.tabBarItem.selectedImage = [[UIImage imageNamed:@"user_select"] imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)];
    [self addChildViewController:userNav];
     */
    [self addSubControllerWithController:[DRActiveViewController new] image:[UIImage imageNamed:@"active"] selectImage:[UIImage imageNamed:@"active_select"] title:@"活动"];
    [self addSubControllerWithController:[DRMovieViewController new] image:[UIImage imageNamed:@"movie"] selectImage:[UIImage imageNamed:@"movie_select"] title:@"电影"];
    [self addSubControllerWithController:[DRCinemaViewController new] image:[UIImage imageNamed:@"cinema"] selectImage:[UIImage imageNamed:@"cinema_select"] title:@"影院"];
    [self addSubControllerWithController:[DRUserHomeViewController new] image:[UIImage imageNamed:@"user"] selectImage:[UIImage imageNamed:@"user_select"] title:@"用户"];
    //修改tabbarItem字体大小颜色
    //非选中状态
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14.0f], NSForegroundColorAttributeName : [UIColor lightGrayColor]} forState:(UIControlStateNormal)];
    //选中状态
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14.0], NSForegroundColorAttributeName : [UIColor cyanColor]} forState:UIControlStateSelected];
    //修改tabbar的颜色
//    [[UITabBar appearance] setBarTintColor:[UIColor orangeColor]];
    [[UITabBar appearance] setBackgroundImage:[self imageWithColor:[UIColor orangeColor]]];
    
    
}
//添加tabbaritem方法
- (void)addSubControllerWithController:(UIViewController *)vc image:(UIImage *)image selectImage:(UIImage *)selectImage title:(NSString *)title{
    //初始化导航控制器
    DRBaseNavViewController *baseNav = [[DRBaseNavViewController alloc] initWithRootViewController:vc];
    baseNav.tabBarItem.title = title;
    baseNav.tabBarItem.image = image;
    baseNav.tabBarItem.selectedImage = [selectImage imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)];
    [self addChildViewController:baseNav];
}
- (UIImage *)imageWithColor:(UIColor *)color{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    //1.绘制大小 2.是否透明 3.缩放比例
    UIGraphicsBeginImageContextWithOptions(rect.size, YES, 1);
    //设置填充
    [color setFill];
    //填充
    UIRectFill(rect);
    //获取到上面填充的图片
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    //结束绘制
    UIGraphicsEndImageContext();
    //返回
    return image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
