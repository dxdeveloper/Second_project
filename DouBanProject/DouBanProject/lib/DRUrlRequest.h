//
//  DRUrlRequest.h
//  DouBanProject
//
//  Created by lanou3g on 16/5/23.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,RequestType) {
    RequestTypeGet,
    RequestTypePost
};
@protocol  DRUrlRequest <NSObject>

//成功

//失败

@end

@interface DRUrlRequest : NSObject

+ (void)getDataWithUrl:(NSString *)url parameters:(NSDictionary *)parameters type:(RequestType)type successBlock:(void(^)(id data))successBlock failedBlock:(void(^)(NSError *error))failedBlock;

@end
