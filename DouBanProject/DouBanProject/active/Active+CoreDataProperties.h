//
//  Active+CoreDataProperties.h
//  DouBanProject
//
//  Created by lanou3g on 16/5/27.
//  Copyright © 2016年 Dragon. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Active.h"

NS_ASSUME_NONNULL_BEGIN

@interface Active (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *address;
@property (nullable, nonatomic, retain) NSString *category_name;
@property (nullable, nonatomic, retain) NSString *content;
@property (nullable, nonatomic, retain) NSString *end_time;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSNumber *wisher_count;
@property (nullable, nonatomic, retain) NSString *begin_time;
@property (nullable, nonatomic, retain) NSString *category;
@property (nullable, nonatomic, retain) NSString *subcategory_name;
@property (nullable, nonatomic, retain) NSNumber *participant_count;
@property (nullable, nonatomic, retain) NSString *image;
@property (nullable, nonatomic, retain) NSSet<Person *> *user;

@end

@interface Active (CoreDataGeneratedAccessors)

- (void)addUserObject:(Person *)value;
- (void)removeUserObject:(Person *)value;
- (void)addUser:(NSSet<Person *> *)values;
- (void)removeUser:(NSSet<Person *> *)values;

@end

NS_ASSUME_NONNULL_END
