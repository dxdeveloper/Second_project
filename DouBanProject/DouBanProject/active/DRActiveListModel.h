//
//  DRActiveListModel.h
//  DouBanProject
//
//  Created by lanou3g on 16/5/23.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DRActiveListModel : NSObject

@property (nonatomic, copy) NSString *address;              // 地址
@property (nonatomic, copy) NSString *begin_time;           // 开始时间
@property (nonatomic, copy) NSString *category;             // 分类
@property (nonatomic, copy) NSString *category_name;        // 分类名
@property (nonatomic, copy) NSString *content;              // 内容
@property (nonatomic, copy) NSString *end_time;             // 结束时间

@property (nonatomic, copy) NSString *has_ticket;           // 是否有票
@property (nonatomic, copy) NSString *image;                // 图片
@property (nonatomic, copy) NSNumber *participant_count;    // 参加人数
//@property (nonatomic, copy) NSString *subcategory_name;     // 子类名
@property (nonatomic, copy) NSString *title;                // 名称
@property (nonatomic, copy) NSNumber *wisher_count;         // 感兴趣的人数



@end
