//
//  Active+CoreDataProperties.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/27.
//  Copyright © 2016年 Dragon. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Active+CoreDataProperties.h"

@implementation Active (CoreDataProperties)

@dynamic address;
@dynamic category_name;
@dynamic content;
@dynamic end_time;
@dynamic title;
@dynamic wisher_count;
@dynamic begin_time;
@dynamic category;
@dynamic subcategory_name;
@dynamic participant_count;
@dynamic image;
@dynamic user;

@end
