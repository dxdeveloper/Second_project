//
//  Person+CoreDataProperties.h
//  DouBanProject
//
//  Created by lanou3g on 16/5/27.
//  Copyright © 2016年 Dragon. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Person.h"

NS_ASSUME_NONNULL_BEGIN

@interface Person (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSSet<Active *> *active;

@end

@interface Person (CoreDataGeneratedAccessors)

- (void)addActiveObject:(Active *)value;
- (void)removeActiveObject:(Active *)value;
- (void)addActive:(NSSet<Active *> *)values;
- (void)removeActive:(NSSet<Active *> *)values;

@end

NS_ASSUME_NONNULL_END
