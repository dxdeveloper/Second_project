//
//  DRDBHelper.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/27.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRDBHelper.h"
#import "DRAppDelegate.h"
#import "Active.h"
#import "DRActiveListModel.h"
#import "Person.h"

@interface DRDBHelper ()
@property (nonatomic, strong) NSManagedObjectContext *context;
@end

@implementation DRDBHelper

- (BOOL)isExistsWithActive:(DRActiveListModel *)listModel user:(NSString *)user{
    NSArray *array = [self searchActiveOfUser:user];
    for (DRActiveListModel *model in array) {
        //收藏过的
        if ([model.title isEqualToString:listModel.title]) {
            return YES;
        }
    }
    //没收藏
    return NO;
}


- (NSManagedObjectContext *)context{
    //获取应用程序代理
    DRAppDelegate *app = [UIApplication sharedApplication].delegate;
    //获取管理上下文
    _context = app.managedObjectContext;
    return _context;
}
- (void)save{
    DRAppDelegate *app = [UIApplication sharedApplication].delegate;
    //保存
    [app saveContext];
}

- (void)insertActive:(DRActiveListModel *)listModel user:(NSString *)user{
    //创建实体描述
    NSEntityDescription *enti = [NSEntityDescription entityForName:@"Active" inManagedObjectContext:self.context];
    //根据实体描述创建一个对象
    Active *active = [[Active alloc] initWithEntity:enti insertIntoManagedObjectContext:self.context];
    //给对象赋值

    active.title = listModel.title;
    active.category = listModel.category;
    active.category_name = listModel.category_name;
    active.begin_time = listModel.begin_time;
    active.end_time = listModel.end_time;
    active.address = listModel.address;
    active.wisher_count = listModel.wisher_count;
    active.image = listModel.image;
    active.content = listModel.content;
    active.participant_count = listModel.participant_count;
    NSEntityDescription *enti1 = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:self.context];
    Person *per = [[Person alloc] initWithEntity:enti1 insertIntoManagedObjectContext:self.context];
    per.name = user;
    [active addUserObject:per];
    [self save];
}

- (NSArray *)searchActiveOfUser:(NSString *)user{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:self.context];
    [fetchRequest setEntity:entity];
    // Specify criteria for filtering which objects to fetch
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name = %@", user];
    [fetchRequest setPredicate:predicate];
    // Specify how the fetched objects should be sorted

    
    NSError *error = nil;
    NSArray *fetchedObjects = [self.context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects.count != 0) {
        NSMutableArray *mArray = [NSMutableArray array];
        //遍历
        for (Person *per in fetchedObjects) {
            //1.NSString name 2.NSSet<Active> active
            for (Active *act in per.active) {
                DRActiveListModel *model = [DRActiveListModel new];
                model.title = act.title;
                model.category = act.category;
                model.category_name = act.category_name;
                model.begin_time = act.begin_time;
                model.end_time = act.end_time;
                model.address = act.address;
                model.wisher_count = act.wisher_count;
                model.image = act.image;
                model.content = act.content;
                model.participant_count = act.participant_count;
                [mArray addObject:model];
            }
        }
        return mArray.copy;
    }
    return nil;
}

@end
