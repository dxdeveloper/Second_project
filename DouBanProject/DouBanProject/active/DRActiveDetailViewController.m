//
//  DRActiveDetailViewController.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/24.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRActiveDetailViewController.h"
#import "DRActiveListModel.h"
#import "DRUserHelper.h"
#import "DRLoginViewController.h"
#import "DRDBHelper.h"


@interface DRActiveDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *showLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation DRActiveDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = NO;
    _titleLabel.text = self.model.title;
    NSString *time = [self.model.begin_time stringByAppendingFormat:@"~%@",self.model.end_time];
    _timeLabel.text = time;
    _userLabel.text =@"dragon";
    _categoryLabel.text = self.model.category_name;
    _addressLabel.text = self.model.address;
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.model.image]];
    UIImage *image = [UIImage imageWithData:data];
    _showLabel.image = image;
    _contentLabel.text = self.model.content;
    
    UIBarButtonItem *shareitem = [[UIBarButtonItem alloc] initWithTitle:@"分享" style:(UIBarButtonItemStyleDone) target:self action:@selector(shareAction)];
    UIBarButtonItem *collectionitem = [[UIBarButtonItem alloc] initWithTitle:@"⭐️" style:(UIBarButtonItemStyleDone) target:self action:@selector(collectionAction)];
    //添加收藏分享按钮
    self.navigationItem.rightBarButtonItems =@[shareitem,collectionitem];
}
- (void)shareAction{
    if ([[DRUserHelper new] isLogin]) { //登录
        
    } else {//未登录
        
    }
}
- (void)collectionAction{
    if ([[DRUserHelper new] isLogin]) { //登录
        //coreData 在该用户下插入一个活动
        if ([[DRDBHelper new] isExistsWithActive:self.model user:[[DRUserHelper new] getUserName]]) {
            //收藏过
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"警告" message:@"该活动已收藏!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
        } else {//没收藏
            UIAlertController *avc = [UIAlertController alertControllerWithTitle:@"恭喜您收藏成功!" message:@"" preferredStyle:(UIAlertControllerStyleAlert)];
            UIAlertAction *alert = [UIAlertAction actionWithTitle:@"确认" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                NSString *user = [[DRUserHelper new] getUserName];
                [[DRDBHelper new] insertActive:self.model user:user];
            }];
            [avc addAction:alert];
            [self presentViewController:avc animated:YES completion:nil];
        }
    } else {//未登录
        [self presentViewController:[DRLoginViewController new] animated:YES completion:nil];
    }
}


@end
