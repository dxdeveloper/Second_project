//
//  DRActiveViewController.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/23.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRActiveViewController.h"
#import "DRUrlRequest.h"
#import "DRActiveListModel.h"
#import "DRActiveHomeTableViewCell.h"
#import "DRActiveDetailViewController.h"

@interface DRActiveViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *mArray;

@end

@implementation DRActiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = NO;
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame style:(UITableViewStylePlain)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    UINib *nib = [UINib nibWithNibName:@"DRActiveHomeTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"cell"];
//    type=all&district=all&loc=108288&photo_cate=image&photo_count=5&start=0&day_type=future&apikey=062bcf31694a52d212836d943bdef876
    NSDictionary *dic = @{@"type":@"all", @"district":@"all",@"loc":@"108288",@"photo_cate":@"image",@"photo_count":@"5",@"start":@"0",@"day_type":@"future",@"apikey":@"062bcf31694a52d212836d943bdef876"};
//    NSLog(@"%@",dic);
//    [DRUrlRequest getDataWithUrl:@"http://api.douban.com/v2/event/list?type=all&district=all&loc=108288&photo_cate=image&photo_count=5&start=0&day_type=future&apikey=062bcf31694a52d212836d943bdef876" parameters:nil type:(RequestTypeGet) successBlock:^(id data) {
//        //字典转模型
//        for (NSDictionary *dic in data[@"events"]) {
//            DRActiveListModel *model = [DRActiveListModel new];
//            [model setValuesForKeysWithDictionary:dic];
//            [self.mArray addObject:model];
//        }
//        //去掉道道
//        if (![self.view.subviews containsObject:self.tableView]) {
//            [self.view addSubview:self.tableView];
//        }
//        //刷新表格
//        [self.tableView reloadData];
//    } failedBlock:^(NSError *error) {
//        NSLog(@"err = %@",error);
//    }];
    [DRUrlRequest getDataWithUrl:@"http://api.douban.com/v2/event/list?" parameters:dic type:(RequestTypeGet) successBlock:^(id data) {
        //字典转模型
        for (NSDictionary *dic in data[@"events"]) {
            DRActiveListModel *model = [DRActiveListModel new];
            [model setValuesForKeysWithDictionary:dic];
           
            [self.mArray addObject:model];
        }
        //去掉道道
        if (![self.view.subviews containsObject:self.tableView]) {
            [self.view addSubview:self.tableView];
        }
        //刷新表格
        [self.tableView reloadData];
    } failedBlock:^(NSError *error) {
        NSLog(@"line = %d, err = %@",__LINE__,error);
    }];
   
    
}
#pragma mark delegate/dataSource
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DRActiveListModel *model = self.mArray[indexPath.row];
    DRActiveDetailViewController *activeDVC = [DRActiveDetailViewController new];
    activeDVC.model = model;
    [self.navigationController pushViewController:activeDVC animated:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 200;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 110;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DRActiveHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    DRActiveListModel *model = self.mArray[indexPath.row];
    cell.title.text = model.title;
    NSString *time = [model.begin_time stringByAppendingFormat:@"~%@",model.end_time];
    cell.startTime.text = time;
    cell.address.text = model.address;
    cell.categoryName.text = model.category_name;
    cell.wisherCount.text = [model.wisher_count stringValue];
    cell.participantCount.text = [model.participant_count stringValue];
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:model.image]];
    cell.listImage.image = [UIImage imageWithData:data];
    return cell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSMutableArray *)mArray{
    if (!_mArray) {
        _mArray = [NSMutableArray array];
    }
    return _mArray;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
