//
//  DRActiveDetailViewController.h
//  DouBanProject
//
//  Created by lanou3g on 16/5/24.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DRActiveListModel;

@interface DRActiveDetailViewController : UIViewController

@property (nonatomic, strong) DRActiveListModel *model;

@end
