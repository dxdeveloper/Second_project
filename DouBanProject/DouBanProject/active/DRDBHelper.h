//
//  DRDBHelper.h
//  DouBanProject
//
//  Created by lanou3g on 16/5/27.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DRActiveListModel;

@interface DRDBHelper : NSObject

- (BOOL)isExistsWithActive:(DRActiveListModel *)listModel user:(NSString *)user;

- (void)insertActive:(DRActiveListModel *)listModel user:(NSString *)user;

- (NSArray *)searchActiveOfUser:(NSString *)user;

@end
