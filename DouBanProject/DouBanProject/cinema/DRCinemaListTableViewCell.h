//
//  DRCinemaListTableViewCell.h
//  DouBanProject
//
//  Created by lanou3g on 16/5/25.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DRCinemaLIstModel;

@interface DRCinemaListTableViewCell : UITableViewCell
@property (nonatomic, strong) DRCinemaLIstModel *model;
@end
