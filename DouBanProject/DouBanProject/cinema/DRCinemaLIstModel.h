//
//  DRCinemaLIstModel.h
//  DouBanProject
//
//  Created by lanou3g on 16/5/25.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DRCinemaLIstModel : NSObject
@property (nonatomic, strong) NSString *cinemaName;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *telephone;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@end
