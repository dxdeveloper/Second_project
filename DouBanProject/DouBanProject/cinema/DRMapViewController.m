//
//  DRMapViewController.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/25.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRMapViewController.h"
#import <MapKit/MapKit.h>
#import "DRAnnotation.h"

@interface DRMapViewController ()<MKMapViewDelegate>
@property (nonatomic, strong) MKMapView *map;
@end

@implementation DRMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.map = [[MKMapView alloc] initWithFrame:self.view.frame];
    self.map.delegate = self;
    //展示用户位置
    self.map.showsUserLocation = YES;
    self.map.rotateEnabled = NO;
    [self.view addSubview:self.map];
    //插大头针
    DRAnnotation *anno = [DRAnnotation new];
    anno.title = self.cinemaTitle;
    anno.coordinate = self.coor;
    anno.annoImage = @"大头针";
    [self.map addAnnotation:anno];
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    if (![annotation isKindOfClass:[DRAnnotation class]]) {
        return nil;
    }
    
    MKAnnotationView *anno= [mapView dequeueReusableAnnotationViewWithIdentifier:@"1"];
    if (!anno) {
        anno = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"1"];
    }
    //执行大头针的经纬度及title
    anno.annotation = annotation;
    anno.image = [UIImage imageNamed:((DRAnnotation *)annotation).annoImage];
    //anno 不能点击
    anno.canShowCallout = YES;
    return anno;
}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    //商家与用户的经纬度差
    CGFloat latitude = ABS(userLocation.coordinate.latitude - self.coor.latitude);
    CGFloat longitude = ABS(userLocation.coordinate.longitude - self.coor.longitude);
    //计算中心点坐标
    CGFloat centerLatitude = (userLocation.coordinate.latitude + self.coor.latitude)/2;
    CGFloat centerlongitude = (userLocation.coordinate.longitude + self.coor.longitude)/2;
    //设置展示范围
    MKCoordinateRegion reg = MKCoordinateRegionMake(CLLocationCoordinate2DMake(centerLatitude, centerlongitude), MKCoordinateSpanMake(latitude+0.05, longitude+0.05));
    [self.map setRegion:reg animated:YES];
    
    //计算两点之间距离
    CLLocation *shopLocation = [[CLLocation alloc] initWithLatitude:self.coor.latitude longitude:self.coor.longitude];
    CLLocation *userLocation1 = [[CLLocation alloc] initWithLatitude:userLocation.coordinate.latitude longitude:userLocation.coordinate.longitude];
    CLLocationDistance distance = [shopLocation distanceFromLocation:userLocation1];
    NSLog(@"%f",distance);
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
