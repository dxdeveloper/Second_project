//
//  DRCinemaViewController.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/23.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRCinemaViewController.h"
#import "DRUrlRequest.h"
#import "DRCinemaLIstModel.h"
#import "DRCinemaListTableViewCell.h"
#import <CoreLocation/CoreLocation.h>
#import "DRMapViewController.h"

@interface DRCinemaViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,CLLocationManagerDelegate>
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *mArray;

@property (nonatomic, strong) CLLocationManager *mgr;

@property (nonatomic, strong) CLGeocoder *geo;

@end

@implementation DRCinemaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //初始化定位管理者
    self.mgr = [CLLocationManager new];
    //判断手机定位是否打开
    if (![CLLocationManager locationServicesEnabled]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"请打开定位" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
        alert.delegate = self;
        //弹出
        [alert show];
    }
    //授权
    if ([[UIDevice currentDevice].systemVersion integerValue] >= 8.0) {
        if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse) {
            //在info 里添加key
            [self.mgr requestWhenInUseAuthorization];
        }
    }
    //设置代理
    self.mgr.delegate = self;
    //设置精度
    self.mgr.desiredAccuracy = 100;
    //设置最小更新距离
    self.mgr.distanceFilter = 10;
    [self.mgr startUpdatingLocation];
    
    
    
    self.navigationController.navigationBar.translucent = NO;
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-120) style:(UITableViewStylePlain)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    //发送网络请求
    [DRUrlRequest getDataWithUrl:@"http://project.lanou3g.com/teacher/yihuiyun/lanouproject/cinemalist.php" parameters:nil type:(RequestTypeGet) successBlock:^(id data) {
        for (NSDictionary *dic in data[@"result"][@"data"]) {
            DRCinemaLIstModel *model = [DRCinemaLIstModel new];
            [model setValuesForKeysWithDictionary:dic];
            [self.mArray addObject:model];
        }
        if (![self.view.subviews containsObject:self.tableView]) {
            [self.view addSubview:self.tableView];
        }
        //刷新表格
        [self.tableView reloadData];
    } failedBlock:^(NSError *error) {
        
    }];
    //注册cell
    [self.tableView registerNib:[UINib nibWithNibName:@"DRCinemaListTableViewCell" bundle:nil] forCellReuseIdentifier:@"DRCinemaListTableViewCell"];

}
#pragma mark 位置管理者
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    [self.mgr stopUpdatingLocation];
    //把经纬度反编码成具体的位置信息
    self.geo = [[CLGeocoder alloc] init];
    //反编码
    [self.geo reverseGeocodeLocation:locations.firstObject completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if (error) {
            return ;
        }
        CLPlacemark *place = placemarks.firstObject;
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:place.locality style:(UIBarButtonItemStylePlain) target:self action:nil];
        self.navigationItem.rightBarButtonItem = item;
        
        
    }];
}

#pragma mark tableViewDelegate/dataSource
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DRMapViewController *mapVC = [DRMapViewController new];
    DRCinemaLIstModel *model = self.mArray[indexPath.row];
    mapVC.coor = CLLocationCoordinate2DMake([model.latitude floatValue], [model.longitude floatValue]);
    mapVC.cinemaTitle = model.cinemaName;
    [self.navigationController pushViewController:mapVC animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DRCinemaListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DRCinemaListTableViewCell" forIndexPath:indexPath];
    cell.model = self.mArray[indexPath.row];
    return cell;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {//取消
        
    } else if(buttonIndex == 1){//确定
        
    }
}
- (NSMutableArray *)mArray{
    if (!_mArray) {
        _mArray = [NSMutableArray array];
    }
    return _mArray;
}


@end
