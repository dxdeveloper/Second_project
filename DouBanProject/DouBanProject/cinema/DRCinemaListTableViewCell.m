//
//  DRCinemaListTableViewCell.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/25.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRCinemaListTableViewCell.h"
#import "DRCinemaLIstModel.h"

@interface DRCinemaListTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@end

@implementation DRCinemaListTableViewCell

- (void)setModel:(DRCinemaLIstModel *)model{
    self.titleLabel.text = model.cinemaName;
    self.addressLabel.text = model.address;
    self.phoneLabel.text = model.telephone;
}

//加载nib
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
