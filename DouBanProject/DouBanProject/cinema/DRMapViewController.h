//
//  DRMapViewController.h
//  DouBanProject
//
//  Created by lanou3g on 16/5/25.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface DRMapViewController : UIViewController

@property (nonatomic, assign) CLLocationCoordinate2D coor;

@property (nonatomic, strong) NSString *cinemaTitle;

@end
