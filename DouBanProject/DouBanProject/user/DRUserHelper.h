//
//  DRUserHelper.h
//  DouBanProject
//
//  Created by lanou3g on 16/5/26.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DRUserHelper : NSObject
//登录
- (void)loginWithUserName:(NSString *)userName password:(NSString *)password withSuccessBlock:(void (^)(id data))successBlock failedBlock:(void(^)(NSError *err))failedBlock;
//注册
- (void)registerWithUserName:(NSString *)userName password:(NSString *)password image:(UIImage *)image withSuccessBlock:(void (^)(id data))successBlock failedBlock:(void(^)(NSError *err))failedBlock;
//注销
- (void)loginOut;
//获取用户名
- (NSString *)getUserName;
//获取用户头像
-  (UIImage *)getUserImage;
//获取用户头像的链接
- (NSString *)getUserImageUrl;
//是否登录状态
- (BOOL)isLogin;
//获取硬盘大小
- (CGFloat)getDiskSize;
//清楚缓存
- (void)clearDisk;
@end
