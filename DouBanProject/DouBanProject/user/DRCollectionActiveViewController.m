//
//  DRCollectionActiveViewController.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/27.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRCollectionActiveViewController.h"
#import "DRDBHelper.h"
#import "DRUserHelper.h"
#import "DRActiveListModel.h"
#import "DRActiveDetailViewController.h"

@interface DRCollectionActiveViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *mArray;
@end

@implementation DRCollectionActiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //加载数据
    self.mArray = [[DRDBHelper new] searchActiveOfUser:[[DRUserHelper new] getUserName]].mutableCopy;
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame style:(UITableViewStylePlain)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
}

#pragma mark tableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:@"cell"];
    }
    //取出数组中模型
    DRActiveListModel *model = self.mArray[indexPath.row];
    //展示在表格上
    cell.textLabel.text = model.title;
    cell.detailTextLabel.text = model.category_name;
    return cell;
}
//跳转详情页
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DRActiveDetailViewController *advc = [DRActiveDetailViewController new];
    advc.model = self.mArray[indexPath.row];
    [self.navigationController pushViewController:advc animated:YES];
}
- (NSMutableArray *)mArray{
    if (!_mArray) {
        _mArray = [NSMutableArray array];
    }
    return _mArray;
}
@end
