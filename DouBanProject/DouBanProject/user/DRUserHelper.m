//
//  DRUserHelper.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/26.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRUserHelper.h"
#import "DRUrlRequest.h"
#import "AFNetworking.h"

@implementation DRUserHelper

//登录
- (void)loginWithUserName:(NSString *)userName password:(NSString *)password withSuccessBlock:(void (^)(id data))successBlock failedBlock:(void(^)(NSError *err))failedBlock{
    [DRUrlRequest getDataWithUrl:@"http://162.211.125.85/douban/user.php/DLogin" parameters:@{@"userName":userName,@"password":password} type:(RequestTypePost) successBlock:^(id data) {
        if ([data[@"code"] integerValue] == 1103) {
            if (successBlock) {
                successBlock(nil);
                //把用户名存一份
                [[NSUserDefaults standardUserDefaults] setObject:userName forKey:@"userName"];
                //把userID存起来
                [[NSUserDefaults standardUserDefaults] setObject:data[@"data"][@"userId"] forKey:@"userId"];
                //图片地址存起来
                [[NSUserDefaults standardUserDefaults] setObject:data[@"data"][@"avatar"] forKey:@"avatar"];
            }
        }else {
//            successBlock(data);
            if ([data[@"code"] integerValue] == 1101) {
                NSString *err = @"登录非法访问";
                successBlock(err);
            }else {
                NSString *err = @"用户不存在或密码错误";
                successBlock(err);
            }
            
        }
        
    } failedBlock:^(NSError *error) {
        NSLog(@"失败");
    }];
}
//注册
- (void)registerWithUserName:(NSString *)userName password:(NSString *)password image:(UIImage *)image withSuccessBlock:(void (^)(id data))successBlock failedBlock:(void(^)(NSError *err))failedBlock{
    AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
    [mgr POST:@"http://162.211.125.85/douban/user.php/DRegister" parameters:@{@"userName":userName,@"password":password} constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        //图片
        
        NSData *data = UIImagePNGRepresentation(image);
        //1.资源对应的key值 2.图片的名字 3.类型
        [formData appendPartWithFileData:data name:@"avatar" fileName:@"shishi" mimeType:@"application/octet-stream"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue] == 1005) {
            if (successBlock) {
                successBlock(responseObject);
                //把userID存起来
                [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"data"][@"userId"] forKey:@"userId"];
                //图片地址存起来
                [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"data"][@"avatar"] forKey:@"avatar"];
            }
        } else {
            NSLog(@"没有注册成功");
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];

}
//注销
- (void)loginOut{
    //从本地把userid userName 头像 删掉
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userName"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"avatar"];
}
//获取用户名
- (NSString *)getUserName{
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"userName"];
}
//获取用户头像
-  (UIImage *)getUserImage{
    NSString *url = [[NSUserDefaults standardUserDefaults] valueForKey:@"avatar"];
    url = [NSString stringWithFormat:@"http://162.211.125.85%@",url];
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    return [UIImage imageWithData:data];
}
//获取用户头像的链接
- (NSString *)getUserImageUrl{
    NSString *url = [[NSUserDefaults standardUserDefaults] valueForKey:@"avatar"];
    url = [NSString stringWithFormat:@"http://162.211.125.85%@",url];
    return url;
}
//是否登录状态
- (BOOL)isLogin{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]) {
        return YES;
    }
    return NO;
}
//获取硬盘大小
- (CGFloat)getDiskSize{
    
    NSString *libPath = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0];
    //初始化文件管理者
    NSFileManager *mgr = [NSFileManager defaultManager];
    //获取lib下所有路径
    NSArray *libAllPath = [mgr subpathsAtPath:libPath];
    NSInteger size = 0;
    for (NSString *path in libAllPath) {
        if (![path containsString:@"Preferences"]) {
            //把路径拼接全
            NSString *pathA = [libPath stringByAppendingPathComponent:path];
            //获取文件的所有信息
            NSDictionary *fileAttri = [mgr attributesOfItemAtPath:pathA error:nil];
            //获取文件大小
            size += [fileAttri[@"NSFileSize"] integerValue];
        }
    }
    return size/1024.0/1024.0;
}
//清楚缓存
- (void)clearDisk{
    NSString *libPath = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0];
    //初始化文件管理者
    NSFileManager *mgr = [NSFileManager defaultManager];
    //获取lib下所有路径
    NSArray *libAllPath = [mgr subpathsAtPath:libPath];

    for (NSString *path in libAllPath) {
        if (![path containsString:@"Preferences"]) {
            //把路径拼接全
            NSString *pathA = [libPath stringByAppendingPathComponent:path];
            //移除文件
            [mgr removeItemAtPath:pathA error:nil];
        }
    }
}















@end
