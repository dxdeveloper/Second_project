//
//  DRUserHomeViewController.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/23.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRUserHomeViewController.h"
#import "DRHeadTableViewCell.h"
#import "DRUserHelper.h"
#import "DRLoginViewController.h"
#import "DRCollectionActiveViewController.h"

@interface DRUserHomeViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSArray *array;

@property (nonatomic, strong) DRUserHelper *helper;

@end

@implementation DRUserHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.array = @[@"我的活动",@"我的电影",@"清除缓存"];
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame style:(UITableViewStylePlain)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
//    注册
    [self.tableView registerNib:[UINib nibWithNibName:@"DRHeadTableViewCell" bundle:nil] forCellReuseIdentifier:@"DRHeadTableViewCell"];
    self.helper = [DRUserHelper new];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"登录" style:(UIBarButtonItemStyleDone) target:self action:@selector(loginAction:)];
    if ([self.helper isLogin]) {
        item.title = @"注销";
    }
    self.navigationItem.rightBarButtonItem = item;
    //添加通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginNofi) name:@"login" object:nil];
}
- (void)loginNofi{
    self.navigationItem.rightBarButtonItem.title = @"注销";
    [self.tableView reloadData];
}
- (void)loginAction:(UIBarButtonItem *)sender{
    if ([self.helper isLogin]) {
        [self.helper loginOut];
        sender.title = @"登录";
        //刷新
        [self.tableView reloadData];
    } else {
        //跳转登录界面
        DRLoginViewController *lvc = [DRLoginViewController new];
        UINavigationController *loginVC = [[UINavigationController alloc] initWithRootViewController:lvc];
        [self presentViewController:loginVC animated:YES completion:nil];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 150;
    }else {
        return 64;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) { //自定义cell
        DRHeadTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DRHeadTableViewCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.imageView.layer.masksToBounds = YES;
        cell.imageView.layer.cornerRadius = 40.0;
        //把登录未登录传给cell
        cell.isLogin = self.helper.isLogin;
        return cell;
    } else { //另外一种cell
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:@"cell"];
        }
//        cell.backgroundColor = [UIColor lightGrayColor];
        cell.textLabel.text = self.array[indexPath.row-1];
        if (indexPath.row == 3) {
            NSString *size = [NSString stringWithFormat:@"%.2fM",[self.helper getDiskSize]];
            cell.detailTextLabel.text = size;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return nil;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 3) {
        UIAlertView *view = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"清除缓存之后可能耗费一点你的流量" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [view show];

    }
    if (indexPath.row == 1) {
        DRCollectionActiveViewController *cavc = [DRCollectionActiveViewController new];
        [self.navigationController pushViewController:cavc animated:YES];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self.helper clearDisk];
        [self.tableView reloadData];
    }
}

@end
