//
//  DRLoginViewController.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/26.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRLoginViewController.h"
#import "DRUserHelper.h"
#import "DRRegisterViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface DRLoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (nonatomic, strong) DRUserHelper *helper;

@end

@implementation DRLoginViewController
- (DRUserHelper *)helper{
    if (!_helper) {
        _helper = [DRUserHelper new];
    }
    return _helper;
}
- (IBAction)registerAction:(id)sender {
    DRRegisterViewController *rvc = [DRRegisterViewController new];
    [self.navigationController pushViewController:rvc animated:YES];
}
- (IBAction)loginAction:(id)sender {
    
    //判断用户名是否合法
    if (self.userNameField.text.length == 0 || self.passwordField.text.length == 0) {
        return;
    } else {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self.helper loginWithUserName:self.userNameField.text password:self.passwordField.text withSuccessBlock:^(id data) {
            if (data) {
                UIAlertController *avc = [UIAlertController alertControllerWithTitle:data message:@"请重新输入..." preferredStyle:(UIAlertControllerStyleAlert)];
                UIAlertAction *alert = [UIAlertAction actionWithTitle:@"确认" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                    for (UIView *view in self.view.subviews) {
                        if ([view isKindOfClass:[MBProgressHUD class]]) {
                            [view removeFromSuperview];
                        }
                    }
                }];
                [avc addAction:alert];
                [self presentViewController:avc animated:YES completion:nil];
                
            } else {
                //发送登录成功的通知
                [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:nil];
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }
            
        } failedBlock:^(NSError *err) {
            
        }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.title = @"登录";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:(UIBarButtonItemStyleDone) target:self action:@selector(back)];
}
//返回
- (void)back{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
