//
//  DRHeadTableViewCell.h
//  DouBanProject
//
//  Created by lanou3g on 16/5/25.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DRHeadTableViewCell : UITableViewCell

@property (nonatomic, assign) BOOL isLogin;

@end
