//
//  DRHeadTableViewCell.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/25.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRHeadTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DRUserHelper.h"

@interface DRHeadTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@end

@implementation DRHeadTableViewCell

- (void)setIsLogin:(BOOL)isLogin{
    if (isLogin) {//登录状态
        //显示用户名和头像
        DRUserHelper *helper = [DRUserHelper new];
        NSString *str = [helper getUserImageUrl];
        NSURL *url = [NSURL URLWithString:str];
        [self.headImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"1111"]];
        self.userNameLabel.text = [helper getUserName];
        
#warning 设置登录
    } else {
        //显示默认头像,label显示未登录
        self.headImage.image = [UIImage imageNamed:@"1111"];
        self.userNameLabel.text = @"未登录";
    }
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
