//
//  DRRegisterViewController.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/26.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRRegisterViewController.h"
#import "DRUserHelper.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface DRRegisterViewController ()
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *rePassword;
@property (nonatomic, strong) DRUserHelper *helper;
@end

@implementation DRRegisterViewController
- (DRUserHelper *)helper{
    if (!_helper) {
        _helper = [DRUserHelper new];
    }
    return _helper;
}
- (IBAction)registerAction:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (_userName.text.length == 0 || _password.text.length == 0 || _rePassword.text == 0) {
        return;
    }
    [self.helper registerWithUserName:self.userName.text password:self.password.text image:[UIImage imageNamed:@"shishi"] withSuccessBlock:^(id data) {
        [self.navigationController popViewControllerAnimated:YES];
    } failedBlock:^(NSError *err) {
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
