//
//  DRMovieListModel.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/26.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRMovieListModel.h"

@implementation DRMovieListModel
- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"images"]) {
        self.smallImage = value[@"medium"];
    }
    if ([key isEqualToString:@"id"]) {
        self.ID = value;
    }
}
@end
