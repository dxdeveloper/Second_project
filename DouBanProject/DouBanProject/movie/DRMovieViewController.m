//
//  DRMovieViewController.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/23.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRMovieViewController.h"
#import "DRUrlRequest.h"
#import "DRMovieTableViewCell.h"
#import "DRMovieListModel.h"
#import "DRMovieDetailViewController.h"

#define MOVIE_LIST_URL @"http://api.douban.com/v2/movie/nowplaying?app_name=doubanmovie&client=e:iPhone4,1%7Cy:iPhoneOS_6.1%7Cs:mobile%7Cf:doubanmovie_2%7Cv:3.3.1%7Cm:PP_market%7Cudid:aa1b815b8a4d1e961347304e74b9f9593d95e1c5&alt=json&city=%E5%8C%97%E4%BA%ACversion=2&start=0&apikey=0df993c66c0c636e29ecbb5344252a4a"

@interface DRMovieViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *mArray;
@end

@implementation DRMovieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = NO;
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame style:(UITableViewStylePlain)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"DRMovieTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"DRMovieTableViewCell"];
    [DRUrlRequest getDataWithUrl:MOVIE_LIST_URL parameters:nil type:(RequestTypeGet) successBlock:^(id data) {
        for (NSDictionary *dic in data[@"entries"]) {
            DRMovieListModel *model = [DRMovieListModel new];
            [model setValuesForKeysWithDictionary:dic];
            [self.mArray addObject:model];
        }
        if (self.mArray) {
            [self.view addSubview:self.tableView];
            [self.tableView reloadData];
        }
    } failedBlock:^(NSError *error) {
        
    }];
    
    
}
#pragma mark tableView代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 120;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DRMovieTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DRMovieTableViewCell" forIndexPath:indexPath];
    cell.model = self.mArray[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DRMovieListModel *model = self.mArray[indexPath.row];
    DRMovieDetailViewController *dvc = [DRMovieDetailViewController new];
    dvc.ID = model.ID;
    [self.navigationController pushViewController:dvc animated:YES];
    
    
}
- (NSMutableArray *)mArray{
    if (!_mArray) {
        _mArray = [NSMutableArray array];
    }
    return _mArray;
}

@end
