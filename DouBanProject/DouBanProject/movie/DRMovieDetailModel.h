//
//  DRMovieDetailModel.h
//  DouBanProject
//
//  Created by lanou3g on 16/5/27.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DRMovieDetailModel : NSObject

/***  标题*/
@property (nonatomic, strong) NSString *title;

/***  放映时间 */
@property (nonatomic, strong) NSString *pubdate;

/***  评分(rating下)*/
@property (nonatomic, strong) NSString *average;

/***  评论人数 rating->details 相加*/
@property (nonatomic, assign) NSInteger persons;

/***  电影时长 durations->[数组]*/
@property (nonatomic, strong) NSString *durations;

/***  国家 countries->[数组]*/
@property (nonatomic, strong) NSString *countries;
/***  类型 genres->[数组]*/
@property (nonatomic, strong) NSString *genres;
/***  内容*/
@property (nonatomic, strong) NSString *summary;
//图片
@property (nonatomic, strong) NSString *image;

@end
