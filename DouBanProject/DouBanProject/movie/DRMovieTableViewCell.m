//
//  DRMovieTableViewCell.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/26.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRMovieTableViewCell.h"
#import "DRMovieListModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DRMovieTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *showImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *starsLabel;
@property (weak, nonatomic) IBOutlet UILabel *pubdateLabel;

@end

@implementation DRMovieTableViewCell

- (void)setModel:(DRMovieListModel *)model{

    self.titleLabel.text = model.title;
    self.starsLabel.text = model.stars;
    self.pubdateLabel.text = model.pubdate;
    [self.showImage sd_setImageWithURL:[NSURL URLWithString:model.smallImage]];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
