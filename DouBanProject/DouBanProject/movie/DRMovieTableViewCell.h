//
//  DRMovieTableViewCell.h
//  DouBanProject
//
//  Created by lanou3g on 16/5/26.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DRMovieListModel;
@interface DRMovieTableViewCell : UITableViewCell
@property (nonatomic, strong) DRMovieListModel *model;
@end
