//
//  DRMovieListModel.h
//  DouBanProject
//
//  Created by lanou3g on 16/5/26.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DRMovieListModel : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *rating;
@property (nonatomic, strong) NSString *pubdate;
@property (nonatomic, strong) NSString *stars;
@property (nonatomic, strong) NSString *smallImage;
@property (nonatomic, strong) NSString *ID;
@end
