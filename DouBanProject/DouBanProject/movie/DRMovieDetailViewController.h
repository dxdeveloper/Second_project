//
//  DRMovieDetailViewController.h
//  DouBanProject
//
//  Created by lanou3g on 16/5/27.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DRMovieDetailViewController : UIViewController
//接收每个电影的id
@property (nonatomic, strong) NSString *ID;

@end
