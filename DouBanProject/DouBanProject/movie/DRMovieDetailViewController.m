//
//  DRMovieDetailViewController.m
//  DouBanProject
//
//  Created by lanou3g on 16/5/27.
//  Copyright © 2016年 Dragon. All rights reserved.
//

#import "DRMovieDetailViewController.h"
#import "DRUrlRequest.h"
#import "DRMovieDetailModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

// 电影详情
#define MOVIE_INFO_URL_1 @"http://api.douban.com/v2/movie/subject/"
// 中间拼接id值
#define MOVIE_INFO_URL_2 @"?apikey=0df993c66c0c636e29ecbb5344252a4a&client=e:iPhone4,1%7Cy:iPhoneOS_6.1%7Cs:mobile%7Cf:doubanmovie_2%7Cv:3.3.1%7Cm:PP_market%7Cudid:aa1b815b8a4d1e961347304e74b9f9593d95e1c5&alt=json&city=%E5%8C%97%E4%BA%AC&version=2&app_name=doubanmovie"

@interface DRMovieDetailViewController ()

@property (nonatomic, strong) NSMutableArray *mArray;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *showImage;
@property (weak, nonatomic) IBOutlet UILabel *commentOfPersonCounts;
@property (weak, nonatomic) IBOutlet UILabel *pubdateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *genresLabel;
@property (weak, nonatomic) IBOutlet UILabel *areaLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryLabel;

@end

@implementation DRMovieDetailViewController

- (NSMutableArray *)mArray{
    if (!_mArray) {
        _mArray = [NSMutableArray array];
    }
    return _mArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //拼接路径
    NSString *url = [NSString stringWithFormat:@"%@%@%@",MOVIE_INFO_URL_1,self.ID,MOVIE_INFO_URL_2];
   
    [DRUrlRequest getDataWithUrl:url parameters:nil type:(RequestTypeGet) successBlock:^(id data) {
       //字典转模型
        DRMovieDetailModel *model = [DRMovieDetailModel new];
        //赋值
        model.title = data[@"title"];
        model.pubdate = data[@"pubdate"];
        model.average = data[@"rating"][@"average"];
        NSDictionary *details = data[@"rating"][@"details"];
        __block NSInteger size = 0;
        [details enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            size += [obj integerValue];
        }];
        model.persons = size;
        model.durations = data[@"durations"][0];
        NSMutableString *countStr = [NSMutableString string];
        for (NSString *coun in data[@"countries"]) {
            [countStr appendString:coun];
            [countStr appendString:@"/"];
        }
        model.countries = [countStr substringToIndex:countStr.length-1].copy;
        NSMutableString *tagStr = [NSMutableString string];
        for (NSString *coun in data[@"genres"]) {
            [tagStr appendString:coun];
            [tagStr appendString:@"/"];
        }
        model.genres = [tagStr substringToIndex:tagStr.length-1].copy;
        model.summary = data[@"summary"];
        model.image = data[@"images"][@"large"];
        //刷新UI
        [self updateUI:model];
    } failedBlock:^(NSError *error) {
        
    }];
}
//更新UI
- (void)updateUI:(DRMovieDetailModel *)model{
    self.scoreLabel.text = [NSString stringWithFormat:@"评分: %@",model.average];
    self.commentOfPersonCounts.text = [NSString stringWithFormat:@"评论人数(%ld)",model.persons];
    self.pubdateLabel.text = [NSString stringWithFormat:@"上映时间: %@",model.pubdate];
    self.timeLabel.text = model.durations;
    self.genresLabel.text = model.genres;
    self.areaLabel.text = model.countries;
    self.summaryLabel.text = model.summary;
    [self.showImage sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"1111"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
